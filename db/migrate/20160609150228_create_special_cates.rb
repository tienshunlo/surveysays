class CreateSpecialCates < ActiveRecord::Migration
  def change
    create_table :special_cates do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
