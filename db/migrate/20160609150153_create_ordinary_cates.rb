class CreateOrdinaryCates < ActiveRecord::Migration
  def change
    create_table :ordinary_cates do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
