class CreateProfiles < ActiveRecord::Migration
  def change
    create_table(:profiles, id: false) do |t|
      t.integer :user_id
      t.string :location
      t.timestamps null: false
    end
      
    add_index :profiles, :user_id, :unique => true
  end
end
