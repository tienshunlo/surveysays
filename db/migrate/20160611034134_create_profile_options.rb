class CreateProfileOptions < ActiveRecord::Migration
  def change
    create_table :profile_options do |t|
      t.integer :profile_id
      t.integer :option_id

      t.timestamps null: false
    end
  end
end
