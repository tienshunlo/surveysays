class CreateResponds < ActiveRecord::Migration
  def change
    create_table :responds do |t|
      t.integer :user_id
      t.integer :option_id

      t.timestamps null: false
    end
  end
end
