class AddBasicOptionToProfile < ActiveRecord::Migration
  def change
   add_column :profiles, :basic_option_a, :integer
   add_column :profiles, :basic_option_b, :integer
   add_column :profiles, :basic_option_c, :integer
   add_column :profiles, :basic_option_d, :integer
   add_column :profiles, :basic_option_e, :integer
   remove_column :profiles, :basic_respond
  end
end
