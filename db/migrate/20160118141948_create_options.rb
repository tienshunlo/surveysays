class CreateOptions < ActiveRecord::Migration
  def change
    create_table :options do |t|
      t.text :content
      t.string :issue_id

      t.timestamps null: false
    end
  end
end
