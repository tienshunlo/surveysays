class CreateCateIssues < ActiveRecord::Migration
  def change
    create_table :cate_issues do |t|
      t.integer :cate_id
      t.integer :issue_id

      t.timestamps null: false
    end
  end
end
