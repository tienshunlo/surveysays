class CreateIssues < ActiveRecord::Migration
  def change
    create_table :issues do |t|
      t.string :content
      t.integer :cate_id

      t.timestamps null: false
    end
  end
end
