class CreateQaSheets < ActiveRecord::Migration
  def change
    create_table :qa_sheets do |t|
      t.integer :answersheet_id
      t.integer :question_id
      t.integer :answer_id

      t.timestamps null: false
    end
  end
end
