$number_of_grades = 0 #integer
$grade_sum = 0.0 #float
$gpa = 0 #integer

    def convert_letterGrade_to_number(letter_grade)
        grade = 0;
        case letter_grade
        when "A"
            grade = 4.0
        when "A-"
            grade = 3.7
        when "B+"
            grade = 3.3
        when "B"
            grade = 3.0
        when "B-"
            grade = 2.7
        when "C+"
            grade = 2.3
        when "C"
            grade = 2.0
        when "C-"
            grade = 2.0
        when "D"
            grade = 1.0
        when "F"
            grade = 0.0
        else 
            puts ""
        end
        grade
    end
 
 def does_user_say_no(quesiton)
     quesiton == "stop"
 end
 
 def get_user_grade_and_add_all
     
     user_input = ""
     #we need to increnment the grade_counter 
     grade_counter = 0
     
     #this part of code will continue to run until the user says no
     
    until does_user_say_no(user_input)
         #where to get the input from the user
         user_input = gets.chomp
         #user input should be convert to a number or a value
         $grade_sum += convert_letterGrade_to_number(user_input)
         grade_counter +=1
     end
     
     #we want to hold the value of the counter
     $number_of_grades = grade_counter -1 
     
     # at the end of the methed, when the calculation is done, we want to return sum of the grade 
     $grade_sum
 end
 
 # the method actually calculate the GPA
 def calculate_gpa
     $gpa = get_user_grade_and_add_all / $number_of_grades
     $gpa #at the end, return the value of GPA.
 end
 
 #For the last part call the method, store the variable say myGrade
 
 myGrade = calculate_gpa
 puts  myGrade
 
 
 
 # def map_it(array)
#i = 0
 #out = []
 #while i < array.size
 #out << yield(array[i])
 #i +=1
 #end
 #out
 #end
 
 #name = ["alice","bob","charlie"]
 #map_it(name) do |item|
  #   item.reverse
 #end
     
 