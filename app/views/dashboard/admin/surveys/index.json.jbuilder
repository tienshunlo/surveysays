json.array!(@surveys) do |survey|
  json.extract! survey, :id, :name
  json.url dashboard_admin_survey_url(survey, format: :json)
end
