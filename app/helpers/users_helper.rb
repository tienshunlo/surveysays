module UsersHelper
  def match_percentage(current_user, user)
    other_user_option = user.option
    number_other_user_option =  other_user_option.length.to_f
    
    current_user_option = current_user.option
    number_current_user_option =  current_user_option.length.to_f
    
    number_same_option = (other_user_option&current_user_option).length.to_f
    
    var_match_current = (number_same_option)/(number_current_user_option)
    var_match_other   = (number_same_option)/(number_other_user_option)
    
    
    @match = number_to_percentage(Math.sqrt(var_match_current * var_match_other)*100, precision: 0)
      
    @match
     

  end
 
 
  def current_user_respond(user_respond,current_user)
  
    @current_user_respond = current_user.issue.find(user_respond.option.issue.id).respond.find_by(:user_id => current_user).option.content
  
    @current_user_respond
  end
end
