class SpecialCate < ActiveRecord::Base
  has_many :cate_issue
  has_many :issue, through: :cate_issue
end
