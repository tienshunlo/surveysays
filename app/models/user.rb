class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_one :answersheet

  has_attached_file :avatar, 
     :styles => { :medium => "300x300>", :thumb => "100x100#", :icon => "50X50#" }, 
     :default_url => "/images/:style/missing.jpg"
  validates_attachment_content_type :avatar, :content_type => /\Aimage\/.*\Z/

#name_scope :without_user, lambda{|user| user ? {:conditions => ["id != ?", user.id]} :{} }
scope :all_except, ->(user) { where.not(id: user) }


   has_many :respond#, inverse_of: :user
   #accepts_nested_attributes_for :respond
   has_many :option, through: :respond
   has_many :issue, through: :option
   
   has_one :profile
end
