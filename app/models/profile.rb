class Profile < ActiveRecord::Base
  belongs_to :user
 
  #可以用的
  has_many :profile_option, dependent: :destroy
  has_many :option, through: :profile_option, dependent: :destroy
  accepts_nested_attributes_for :profile_option
  #可以用的
  
  
  self.primary_key = 'user_id'
  GENDER_TYPES = [ "Male", "Female", "Do not wish to say" ]
  
  #validates_presence_of :location
  #validates_presence_of :gender

end
