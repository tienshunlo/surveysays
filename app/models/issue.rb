class Issue < ActiveRecord::Base
    has_many :cate_issue
    has_many :cate,             through: :cate_issue
    has_many :ordinary_cate,    through: :cate_issue
    has_many :special_cate,     through: :cate_issue
    
    
    #belongs_to :cate
    has_many :option, :dependent => :destroy
    has_many :respond, through: :option
    has_many :user, through: :respond
    
    accepts_nested_attributes_for :option, :reject_if => lambda { |a| a[:content].blank? }, :allow_destroy => true
    
    
     #<% cities_array = City.all.map { |city| [city.name, city.id] } %>
  #<%= options_for_select(cities_array) %>
  
  GENDER_TYPES = [ "Male", "Female", "Do not wish to say" ]
  
  CATES_ARRAY = Cate.all.collect {|p| [ p.name, p.id ] }
  ORDINARY_CATES_ARRAY = OrdinaryCate.all.collect {|p| [ p.name, p.id ] }
  SPECIAL_CATES_ARRAY = SpecialCate.all.collect {|p| [ p.name, p.id ] }
  
  #cates_array = Cate.all.collect {|p| [ p.name, p.id ] }
  #ordinarycates_array = OrdinaryCate.all.collect {|p| [ p.name, p.id ] }
  #specialcates_array = SpecialCate.all.collect {|p| [ p.name, p.id ] }
end