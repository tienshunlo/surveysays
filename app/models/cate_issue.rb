class CateIssue < ActiveRecord::Base
  belongs_to :ordinary_cate
  belongs_to :special_cate
  belongs_to :cate
  belongs_to :issue
end
