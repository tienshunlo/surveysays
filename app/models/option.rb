class Option < ActiveRecord::Base
    belongs_to :issue
    has_many :respond,  :dependent => :destroy
    has_many :user,     through: :respond
    
    #可以用的
    has_many :profile_option
    has_many :profile,  through: :profile_option
    #可以用的
end
