class Answersheet < ActiveRecord::Base
belongs_to :user
has_many :qa_sheet
has_many :question, through: :qa_sheet
has_many :answer,   through: :qa_sheet
#accepts_nested_attributes_for :questions

end
