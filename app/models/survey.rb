class Survey < ActiveRecord::Base
  has_many :question, :dependent => :destroy
  accepts_nested_attributes_for :question, :reject_if => lambda { |a| a[:content].blank? }, :allow_destroy => true
  
  has_many :questionnaire_survey
  has_many :questionnaire, through: :questionnaire_survey
  
end