class Question < ActiveRecord::Base
	belongs_to :survey
	has_many :answer, :dependent => :destroy
	accepts_nested_attributes_for :answer, :reject_if => lambda { |a| a[:content].blank? }, :allow_destroy => true

    
    has_many :answersheet, through: :qa_sheet

end
