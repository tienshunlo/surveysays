class  RespondsController < ApplicationController
   
    before_action :find_respond, only: [:show, :edit, :update, :destroy]
    before_action :find_user
    
    
    
    def index
        #回答過的問題
        @responded_issue      = @user.respond.all
        #@responded_issue      = Respond.where(id: @user.respond.map{|t| t.id}
        #未答過的問題
        @unresponded_issue  = Issue.where.not(id: @user.issue.map{|t| t.id} )
    end
    
    def responded_issue
        @responded_issue      = current_user.respond.all.order('updated_at DESC')
    end
    
    def unresponded_issue
        @unresponded_issue  = Issue.where.not(id: @user.issue.map{|t| t.id})
    end
    
   
    
   
    
    
    
    private
    
    def find_user
        @user = current_user
    end
    
    
    def respond_params
        params.require(:respond).permit(:user_id, :option_id)
    end
    
    def find_respond
        @respond = Respond.find(params[:id])
    end
    
end
