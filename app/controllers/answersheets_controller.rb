class AnswersheetsController < ApplicationController
	

    before_action :find_user, only: [:show, :new, :edit, :create,:update] 
    before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy] 
 


	def show
		@answersheet = @user.answersheet
	end

	def new
        
        @answersheet = @user.build_answersheet
		#@answersheet = Answersheet.find(params[:user_id])
		@question = Question.all

	end
	 
	def edit
        
        
	end

	def create
		@answersheet = @user.build_answersheet(answersheet_params)
		if @answersheet.save
          redirect_to user_path(@user)
        else
          render :new
        end
	end

	def update
	   @answersheet = @user.answersheet

	   if @answersheet.update(answersheet_params)
	     redirect_to user_path(@user), notice: "文章修改成功！"
	   else
	     render :edit
	  end
	end

	def destroy
	end
private
    
    def answersheet_params
      params.require(:answersheet).permit(:title, :answer_ids =>[])
    end

    private

  def find_user
      @user = User.find(params[:user_id])
  end  

end
