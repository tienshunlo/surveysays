class  Dashboard::RespondsController < Dashboard::DashboardController 
   
    before_action :find_respond, only: [:show, :edit, :update, :destroy]
    
    
    
    def index
        #回答過的問題
        @responded_issue      = @user.respond.all
        #@responded_issue      = Respond.where(id: @user.respond.map{|t| t.id}
        #未答過的問題
        @unresponded_issue  = Issue.where.not(id: @user.issue.map{|t| t.id} )
    end
    
    def responded_issue
        @responded_issue      = current_user.respond.all.order('updated_at DESC')
    end
    
    def unresponded_issue
        @unresponded_issue  = Issue.where.not(id: @user.issue.map{|t| t.id})
    end
    
    def special_cates
        @respond = @user.respond.new
        @special_cates = SpecialCate.find_by(:id => 4)
        @issues = @special_cates.issue
        
    end
    
   
    
    def new
        @respond = @user.respond.build
    	@issue = Issue.find(params[:q])
    end
    
    
    def edit
       
        @respond = Respond.find(params[:id])
    end
    
    def create 
       @respond = @user.respond.new(respond_params)
       if @respond.save
           redirect_to unresponded_issue_dashboard_user_responds_path
       else
           render :new
       end
    end
    
    def update
        if @respond.update(respond_params)
            redirect_to responded_issue_dashboard_user_responds_path
        else
           render :edit
        end
    end
    
    
    
    
    private
    
    
    def respond_params
        params.require(:respond).permit(:user_id, :option_id)
    end
    
    def find_respond
        @respond = Respond.find(params[:id])
    end
    
end
