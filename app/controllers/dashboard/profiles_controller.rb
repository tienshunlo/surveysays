class  Dashboard::ProfilesController < Dashboard::DashboardController 
  before_action :find_user
  before_action :set_profile
   
  
  
  
  def new
     @profile = @user.build_profile
     #id:4是登入類別
     @issues = SpecialCate.find(4).issue
    
     
  end
  
  
  def create
    @profile = @user.build_profile(profile_params)
    if @profile.save
      #@profile_option = @profile.profile_option.create(:id => current_user ,:profile_id => current_user)
      #redirect_to  dashboard_user_path(current_user)
      redirect_to unresponded_issue_dashboard_user_responds_path
      #redirect_to special_cates_dashboard_user_responds_path(current_user) 測試直接寫fields_for的方法寫respond，結果寫不出來
    else 
      render new
    end
  end
  
  def edit
   
  end
  
  def update
    
    
    
    ids = params[:profile][:profile_option_attributes].map{|t| t[:option_id]}
    #ids = (params[:ic_old] || {}).keys.map{|i|i.to_i}
    if ids.length > 0
      ProfileOption.where("profile_id =? AND option_id NOT IN (#{ids.join(',')})", @profile.id).delete_all
    else
      ProfileOption.where("profile_id = ?", @profile.id).delete_all
    end
    
    #更新舊的，不過這個APP不需要
    #@profile.profile_option.each do |f|
     # ic = ProfileOption.where(:profile_id => @profile.id, :id => f.id).first
      #if ic
       # ic.update_attributes(:option_id => f.option_id)
      #end
    #end
    
    #加入新的作法一
    params[:profile][:profile_option_attributes].each do |index|
      if !@profile.profile_option.map(&:option_id).include?(index[:option_id].to_i)
        ProfileOption.create(:profile_id => @profile.id, :option_id => index[:option_id])
      end
    end
    
    #加入新的作法二
    #params[:profile][:profile_option_attributes].each_with_index do |tar, index|
     # if !@profile.profile_option.map(&:option_id).include?(tar[:option_id].to_i)
      #  ProfileOption.create(:profile_id => @profile.id, :option_id => tar[:option_id])
      # end
    #end
    
    redirect_to  dashboard_user_path(current_user)
    
    
    #scattfold的普通版本
    #if @profile.update(profile_params)
        #redirect_to  dashboard_user_path(current_user)
    #else
        #render edit
    #end
        
  end
  
  private
  def profile_params
    
   
    #可以用的
    params.require(:profile).permit(:location, :gender, profile_option_attributes:[:id, :option_id])
    #可以用的
    
    #params.require(:profile).permit(:location, :gender, :option_ids => [])
    #params.require(:profile).permit(:location, :gender, :option_ids => [], :basic_option_a, :basic_option_b, :basic_option_c, :basic_option_d, :basic_option_e)

  end
  
  def find_user
    @user = current_user
  end
  
  def set_profile
    @profile = @user.profile
  end
  
end
