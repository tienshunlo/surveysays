class  Dashboard::QuestionsController < Dashboard::DashboardController 

    before_action :find_user, only: [:show, :new, :edit, :create,:update] 
    before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy] 
 

   


	
	def edit
        @user = User.find(params[:user_id])
        @question = Question.find(params[:id])
        
	end

	

	def update
	   @question = Question.find(params[:id])

	   if @question.update(question_params)
	     redirect_to dashboard_user_answersheet_path, notice: "文章修改成功！"
	   else
	     render :edit
	  end
	end

	def destroy
	end

    
   
   private
    
    def question_params
      params.require(:question).permit(:title, :answer_ids =>[])
    end

    

    def find_user
      @user = User.find(params[:user_id])
    end 

    

end
