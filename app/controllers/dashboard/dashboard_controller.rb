class Dashboard::DashboardController < ApplicationController
	before_action :authenticate_user!
	before_action :find_user
	#before_action :find_user, only: [:show, :new, :edit, :create,:update] 
	
	layout 'dashboard'
	
	private
	def find_user
      @user = current_user 
    end  
    
end