class Dashboard::Admin::CatesController < Dashboard::Admin::AdminController 
    
    def index
        @cates = Cate.all
        
    end
        
    def new
        @cate = Cate.new
    end
    
    def edit
        @cate = Cate.find(params[:id])
    end
    
    def create
        @cate = Cate.new(cate_params)
        if @cate.save
            redirect_to dashboard_admin_cates_path
        else
            render :new
        end
    end
    
    def update
        @cate = Cate.find(params[:id])
        
         if @cate.update(cate_params)
            redirect_to dashboard_admin_cates_path
        else
            render :edit
        end 
    end
    
    
    def destroy
      @cate = Cate.find(params[:id])
      @cate.destroy
      redirect_to dashboard_admin_cates_path, alert: "項目已刪除"
    end
    
    private
     def cate_params
         params.require(:cate).permit(:name)
     end
end
