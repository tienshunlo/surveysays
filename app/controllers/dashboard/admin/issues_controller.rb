class Dashboard::Admin:: IssuesController < Dashboard::Admin::AdminController 
    def index
        @issues = Issue.all
    end
    
    def new
        @issue = Issue.new
        4.times { @issue.option.build }
    end
    
    
    def edit
        @issue = Issue.find(params[:id])
    end
    
    def create
        @issue = Issue.new(issue_params)
        if @issue.save
            redirect_to dashboard_admin_issues_path
        else
            render :new
        end
    end
    
    def update
        @issue = Issue.find(params[:id])
        if @issue.update(issue_params)
            redirect_to dashboard_admin_issues_path
        else
            render :edit
        end
    end
    
    def destroy 
        @issue = Issue.find(params[:id])
        @issue.destroy
        redirect_to dashboard_admin_issues_path
        
    end
    
    private
    def issue_params
        params.require(:issue).permit(:content, :cate_ids => [], :ordinary_cate_ids => [], :special_cate_ids => [], option_attributes:[:issue_id, :id, :content,:_destroy])
    end
    
end
