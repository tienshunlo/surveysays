class Dashboard::Admin::SpecialCatesController < Dashboard::Admin::AdminController 
    
    def index
        @specialcates = SpecialCate.all
        
    end
        
    def new
        @specialcate = SpecialCate.new
    end
    
    def edit
        @specialcate = SpecialCate.find(params[:id])
    end
    
    def create
        @specialcate = SpecialCate.new(specialcate_params)
        if @specialcate.save
            redirect_to dashboard_admin_special_cates_path
        else
            render :new
        end
    end
    
    def update
        @specialcate = SpecialCate.find(params[:id])
        
         if @specialcate.update(specialcate_params)
            redirect_to dashboard_admin_special_cates_path
        else
            render :edit
        end 
    end
    
    
    def destroy
      @specialcate = SpecialCate.find(params[:id])
      @specialcate.destroy
      redirect_to dashboard_admin_special_cates_path, alert: "項目已刪除"
    end
    
    private
     def specialcate_params
         params.require(:special_cate).permit(:name)
     end
end
