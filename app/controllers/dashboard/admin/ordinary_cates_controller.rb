class Dashboard::Admin::OrdinaryCatesController < Dashboard::Admin::AdminController 
    
    def index
        @ordinarycates = OrdinaryCate.all
        
    end
        
    def new
        @ordinarycate = OrdinaryCate.new
    end
    
    def edit
        @ordinarycate = OrdinaryCate.find(params[:id])
    end
    
    def create
        @ordinarycate = OrdinaryCate.new(ordinarycate_params)
        if @ordinarycate.save
            redirect_to dashboard_admin_ordinary_cates_path
        else
            render :new
        end
    end
    
    def update
        @ordinarycate = OrdinaryCate.find(params[:id])
        
         if @ordinarycate.update(ordinarycate_params)
            redirect_to dashboard_admin_ordinary_cates_path
        else
            render :edit
        end 
    end
    
    
    def destroy
      @ordinarycate = OrdinaryCate.find(params[:id])
      @ordinarycate.destroy
      redirect_to dashboard_admin_ordinary_cates_path, alert: "項目已刪除"
    end
    
    private
     def ordinarycate_params
         params.require(:ordinary_cate).permit(:name)
     end
end

