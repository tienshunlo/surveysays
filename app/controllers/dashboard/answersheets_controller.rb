class  Dashboard::AnswersheetsController < Dashboard::DashboardController 

    before_action :find_user, only: [:show, :new, :edit, :create,:update] 
    before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy] 
 

   


	def show
		@answersheet = @user.answersheet
		@question = Question.all
	end

	def new        
    @answersheet = @user.build_answersheet(:user_id => current_user.id)
		@question = Question.all
  end

	 
	def edit
       
	end

	def create

		@answersheet = @user.build_answersheet(answersheet_params)
		if @answersheet.save
			redirect_to dashboard_user_answersheet_path(@user, @answersheet)
		else
			render :new
		end
	end

	def update
	   @answersheet = Answersheet.find(params[:id])

	   if @answersheet.update(answersheet_params)
	     redirect_to dashboard_user_answersheet_path, notice: "文章修改成功！"
	   else
	     render :edit
	  end
	end

	def destroy
	end

    def multi
	    
	   
	    @qasheet = QaSheet.where(:answersheet_id => current_user.answersheet.id)
	    @question = Question.all

	end
	  
	def multi_save


	    #取出所有關連id
	    ids = (params[:as_old] || {}).keys.map{|i|i.to_i}

	    #語意：排除送出的id之外的所有隸屬item的都刪除
	    QaSheet.where("answersheet_id = #{current_user.answersheet.id} AND id NOT IN (#{ids.join(',')})").delete_all

	    #更新舊的資料
	    if params[:as_old]
	      params[:as_old].each_pair do |id , data|
	        ic = QaSheet.where(:answersheet_id => current_user.answersheet.id , :id => id).first
	        if ic
	          #這邊要過 permit 或是一個一個指定都行
	          ic.update_attributes(:answer_id => data[:id])
	        end
	      end
	    end

	    #額外新增的都再塞入
	    if params[:as_new]
	      params[:as_new][:answer_ids].each_index do |index|
	        QaSheet.create(:answersheet_id => current_user.answersheet.id , :answer_ids => params[:as_new][:answer_ids][index])
	      end
	    end
	    redirect_to users_path
	end
    
    
   
   private
    
    def answersheet_params
      params.require(:answersheet).permit(:user_id, :title, :answer_ids =>[])
    end

    

    def find_user
      @user = User.find(params[:user_id])
    end 

    

end
