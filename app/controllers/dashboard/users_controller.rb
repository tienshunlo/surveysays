class  Dashboard::UsersController < Dashboard::DashboardController 

    #before_action :find_user, only: [:show, :new, :edit, :create,:update] 
    before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy] 
 

   def index
		@user = @paginate = User.paginate(:page => params[:page])
		#@items = @paginate = Item.paginate(:page => params[:page])
		#@answersheet = @paginate = @user.answersheet.paginate(:page => params[:page])
		#@answersheet = @paginate = Answersheet.paginate(:page => params[:page])
	end


	def show
	  @responded_issue      = @paginate = @user.respond.all.paginate(:page => params[:page], :per_page => 10)
	  @issues = SpecialCate.find(4).issue
	  @profile = @user.profile
	end


  private
  def answersheet_params
  	params.require(:answersheet).permit(:title, :answer_ids =>[])
  end


  #def find_user
      #@user = current_user
  #end  

end
