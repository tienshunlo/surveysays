class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :configure_permitted_parameters, if: :devise_controller?
  
protected

   def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) { |u| u.permit(:name, :email, :password, :password_confirmation) }
    devise_parameter_sanitizer.for(:account_update) { |u| u.permit(:name, :email, :password, :password_confirmation, :current_password,:avatar) }
   end

   def after_sign_in_path_for(resource) 
    case resource 
      when User 
        #dashboard_user_path(resource)
        #如果沒有profile，去新增一個。
        if current_user.profile.blank?
         new_dashboard_user_profile_path(resource)
        else
          dashboard_user_path(resource)
        end
        #如果profile驗證有錯誤，去edit profile
        #if current_user.profile.valid?
          #edit_dashboard_user_profile_path(resource)
        #else
          #root_path
        #end
      when Manager
        dashboard_admin_managers_path
      else
        root_path 
    end 
    
    
    
    
  end
  

end
