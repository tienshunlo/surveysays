class IssuesController < ApplicationController
    def show
        @issue = Issue.find(params[:id])
        #@user = @issue.user 
        @user = @paginate = @issue.user.all_except(current_user).paginate(:page => params[:page], :per_page => 12)
    end
    
end
