Rails.application.routes.draw do
  
  devise_for :managers
  
  root 'users#index'

  devise_for :users
  
  #resources :questionnaires
  #resources :surveys 
  
  resources :users #do
    #resources :answersheets
  #end
  
  resources :responds do 
        collection do
          get :responded_issue
          get :unresponded_issue
        end
  end
  
  resources :items , :except => [:show] do
    member do
      get :multi
      post :multi_save
    end
  end
  
  resources :issues, :only => [:show]
  
  #get 'responded_issue'  => 'responds#responded_issue'
  #get 'respond'  => 'surveys#respond'

  namespace :dashboard do
    resources :users do
      resources :issues
      resources :options
      resources :profiles 
      resources :responds do 
        collection do
          get   :responded_issue
          get   :unresponded_issue
          get   :special_cates
          post  :special_cates_save
        end
      end
    end
    namespace :admin do 
      resources :users
      resources :managers
      #resources :questionnaires
      #resources :surveys
      resources :cates
      resources :ordinary_cates
      resources :special_cates
      resources :issues
      resources :options
     end
   end



  

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
